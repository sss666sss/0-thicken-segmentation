clear; close all;
addpath(genpath('.'));

% % Landmarks labels

% Chehra
lm_chehra.LeftEyebrow = 1:5;
lm_chehra.RightEyebrow = 6:10;
lm_chehra.LeftEye = 20:25;
lm_chehra.RightEye = 26:30;
lm_chehra.Nose = [11, 15, 19]; % 11:14;
% lm_chehra.Nostrils = 15:19;
lm_chehra.OutterMouth = 32:43;
% lm_chehra.InnerMouth = 44:49;

lm_pos = lm_chehra;

% % Test Path
image_path='test_images/';
img_list=dir([image_path,'*.jpg']);

% % Load Models
fitting_model='models/Chehra_f1.0.mat';
load(fitting_model);    

% % Perform Fitting
for i=1:size(img_list,1)

    % % Load Image
    test_image=im2double(imread([image_path,img_list(i).name]));
    figure1=figure;
    imshow(test_image);hold on;
    
    disp(['Detecting Face in ' ,img_list(i).name]);
    faceDetector = vision.CascadeObjectDetector();
    bbox = step(faceDetector, test_image);
    
    % draw face detector's rectangle
    %     test_image = insertShape(test_image,'rectangle',bbox);
    %     imshow(test_image)
    
    test_init_shape = InitShape(bbox,refShape);
    test_init_shape = reshape(test_init_shape,49,2);
    
    % first approximation of landmarks: by relocating the model according
    % to the face detection location
    %     plot(test_init_shape(:,1),test_init_shape(:,2),'ro');
    
    % plot numbers
    %     c = strsplit(num2str(1:numel(test_init_shape(:,1))), ' ');
    %     dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
    %     text(test_init_shape(:,1)+dx, test_init_shape(:,2)+dy, c);
    
    if size(test_image,3) == 3
        test_input_image = im2double(rgb2gray(test_image));
    else
        test_input_image = im2double((test_image));
    end
    
    disp(['Fitting ' img_list(i).name]);    
    % % Maximum Number of Iterations 
    % % 3 < MaxIter < 7
    MaxIter=6;
    landmarks = Fitting(test_input_image,test_init_shape,RegMat,MaxIter);
    
%     landmarks = zeros(size(test_image,1), size(test_image,2));
%     rows = round(test_points(:,2));
%     cols = round(test_points(:,1));
%     idx = rows + (cols-1)*size(test_image,1);
%     landmarks(idx) = 1;
%     imwrite(landmarks, strcat(image_path, strrep(img_list(i).name, '.jpg', ''), '_landmarks.jpg'));
    
    % create masks for each facial component and draw limits
    masks = createMasks(test_input_image, landmarks, lm_pos);  
    [cont_i, cont_j] = find(masks.total_outter_border);
    plot(cont_j, cont_i, 'b.')

    plot(landmarks(:,1),landmarks(:,2),'g*','MarkerSize',3);hold off;
    legend('Initialization','Final Fitting');
    set(gcf,'units','normalized','outerposition',[0 0 1 1]);
    saveas(figure1,[image_path,strrep(img_list(i).name, '.jpg', '_thInf.jpg')]);
    pause;
    close all;
    
end
